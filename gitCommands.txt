Pre-Work/Certificates/HTML/CodeAcademy

<Username>: saafir01
<Email>: saafir01@gmail
<Folder Name>:
<Repository URL>: https://saafir01@bitbucket.org/saafir01/swcguild.git

- - - Repository Creation
* * * * * * * * * * * * * * * * * * * * *
git init
	- Create a git repository (.git)
	- If a git repository is not created by running this command, the next commands
	  will issue a fatal.
	- fatal: Not a git repository (or any parent directories): .git

git config -global user.name "<Username>"
git config -global user.email "<Email>"

git remote add origin <Repository URL>

- - - Repository Tracking
* * * * * * * * * * * * * * * * * * * * *
git push -u origin master
	- Option to ensure tracking to be performed against the remote repository

- - - Repository Cloning
* * * * * * * * * * * * * * * * * * * * *
git clone <Repository URL> <Folder Name>
	- Cloning fault will occur if you have access restrictions to the URL repository


- - - Adding Files to the Repository
* * * * * * * * * * * * * * * * * * * * *
git add --all
git add <File Name>

git commit -m "<Message Comment>"

git pull origin master

git push origin master


- - - Accessing the Repository
* * * * * * * * * * * * * * * * * * * * *
git config --global htp.sslVerify false
	- Unable to access the repository due to a SSL certificate problem



- - - Repository Status
* * * * * * * * * * * * * * * * * * * * *

git status

git log --oneline --graph --decorate
